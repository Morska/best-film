    <footer class="footer">
      <div class="footer__container">
        <a class="footer__logo" href=""></a>
        <div class="footer__content">
          <address class="address address--name">
            <?php echo get_field('company_name', 'option'); ?>
          </address>
          <address class="address address--address">
            <?php echo get_field('company_address', 'option'); ?>
          </address>
          <address class="address address--tel">
            tel.&nbsp;<?php echo get_field('company_phone', 'option'); ?> | fax&nbsp;<?php echo get_field('company_fax', 'option'); ?>
          </address>
          <address class="address address--reg">
            NIP&nbsp;<?php echo get_field('company_nip', 'option'); ?> | REGON&nbsp;<?php echo get_field('company_regon', 'option'); ?>
          </address>
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
