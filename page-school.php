<?php /* Template Name: Page School */ ?>
<?php get_header(); ?>
<?php set_query_var('cat', 'szkola'); ?>

<main class="main">

<?php get_template_part('partials/section', 'hero'); ?>
<?php get_template_part('partials/section', 'films'); ?>

</main>

<?php get_footer(); ?>
