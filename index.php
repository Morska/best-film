<?php /* Template Name: Index */ ?>
<?php get_header(); ?>
<?php set_query_var('cat', 'kino'); ?>

<main class="main">

<?php get_template_part('partials/section', 'slider'); ?>
<?php get_template_part('partials/section', 'films'); ?>

</main>

<?php get_footer(); ?>
