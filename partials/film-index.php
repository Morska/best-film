<?php
  $postID = get_the_ID();
  $s_url = get_the_post_thumbnail_url($post_id, 'slider-s');
  $thumbnail_id = get_post_thumbnail_id($post_id);
  $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
?>

<article class="film film--index">
  <a class="film__link" href="<?php the_permalink(); ?>">
    <picture class="film__img">
      <source media="(max-width: 768px)" srcset="<?php echo $s_url; ?>">
      <img src="<?php echo $s_url; ?>" alt="<?php echo $alt; ?>">
    </picture>
    <h2 class="film__title"><?php the_title(); ?></h2>
  </a>
</article>
