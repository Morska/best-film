<section class="main__section section--filmy">
  <div class="section__container">

<?php
$cat_name = get_query_var('cat');
$args = array(
  'post_type' => 'film',
  'post_status' => 'publish',
  'hide_empty' => false,
  'offset' => 0,
  'order' => 'DESC',
  'posts_per_page' => 8,
  'tax_query' => array(
    array(
      'taxonomy' => 'film_category',
      'field'    => 'slug',
      'terms'    => $cat_name,
    ),
  )
);
$the_query = new WP_Query($args);
while ($the_query->have_posts()) :
  $the_query->the_post();
  get_template_part('partials/film', 'index');
  endwhile;
  wp_reset_postdata();
?>

  </div>
</section>
