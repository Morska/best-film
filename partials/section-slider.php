<section class="slider slider--header">

<?php
wp_reset_query();
$cat_id = get_cat_ID('slider');
$args = array(
  'post_type' => 'post',
  'post_status' => 'publish',
  'hide_empty' => false,
  'offset' => 0,
  'order' => 'DESC',
  'cat' => array($cat_id),
  'posts_per_page' => 5
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) : while($the_query->have_posts()) : $the_query->the_post();
  $post_id = get_the_ID();
  $s_url = get_the_post_thumbnail_url($post_id, 'slider-s');
  $m_url = get_the_post_thumbnail_url($post_id, 'slider-m');
  $d_url = get_the_post_thumbnail_url($post_id, 'slider-d');
  $thumbnail_id = get_post_thumbnail_id($post_id);
  $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
?>

  <div class="slider__slide">
    <picture class="slider__img">
      <source media="(max-width: 768px)" srcset="<?php echo $s_url; ?>">
      <source media="(max-width: 1365px)" srcset="<?php echo $m_url; ?>">
      <source media="(min-width: 1366px)" srcset="<?php echo $d_url; ?>">
      <img src="<?php echo $s_url; ?>" alt="<?php echo $alt; ?>">
    </picture>
  </div>

<?php
  endwhile;
  endif;
  wp_reset_postdata();
?>

</section>
