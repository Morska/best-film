<?php

function film_category() {
  $labels = array(
    'name'                       => _x( 'Film categories', 'Taxonomy General Name', 'arcanagis' ),
    'singular_name'              => _x( 'Film category', 'Taxonomy Singular Name', 'arcanagis' ),
    'menu_name'                  => __( 'Film categories', 'arcanagis' ),
    'all_items'                  => __( 'All Film categories', 'arcanagis' ),
    'parent_item'                => __( 'Parent Film category', 'arcanagis' ),
    'parent_item_colon'          => __( 'Parent Film category:', 'arcanagis' ),
    'new_item_name'              => __( 'New Film category', 'arcanagis' ),
    'add_new_item'               => __( 'Add New Film category', 'arcanagis' ),
    'edit_item'                  => __( 'Edit Film category', 'arcanagis' ),
    'update_item'                => __( 'Update Film category', 'arcanagis' ),
    'view_item'                  => __( 'View Film category', 'arcanagis' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'arcanagis' ),
    'add_or_remove_items'        => __( 'Add or remove Film categories', 'arcanagis' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'arcanagis' ),
    'popular_items'              => __( 'Popular Film categories', 'arcanagis' ),
    'search_items'               => __( 'Search Film categories', 'arcanagis' ),
    'not_found'                  => __( 'Not Found', 'arcanagis' ),
    'no_terms'                   => __( 'No Film categories', 'arcanagis' ),
    'items_list'                 => __( 'Items list', 'arcanagis' ),
    'items_list_navigation'      => __( 'Items list navigation', 'arcanagis' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'film_category', array( 'film' ), $args );

}
add_action( 'init', 'film_category', 0 );

function wpdocs_codex_book_init() {
  $labels = array(
    'name'                  => _x( 'Films', 'Post type general name', 'textdomain' ),
    'singular_name'         => _x( 'Film', 'Post type singular name', 'textdomain' ),
    'menu_name'             => _x( 'Films', 'Admin Menu text', 'textdomain' ),
    'name_admin_bar'        => _x( 'Film', 'Add New on Toolbar', 'textdomain' ),
    'add_new'               => __( 'Add New', 'textdomain' ),
    'add_new_item'          => __( 'Add New Film', 'textdomain' ),
    'new_item'              => __( 'New Film', 'textdomain' ),
    'edit_item'             => __( 'Edit Film', 'textdomain' ),
    'view_item'             => __( 'View Film', 'textdomain' ),
    'all_items'             => __( 'All Films', 'textdomain' ),
    'search_items'          => __( 'Search Films', 'textdomain' ),
    'parent_item_colon'     => __( 'Parent Films:', 'textdomain' ),
    'not_found'             => __( 'No Films found.', 'textdomain' ),
    'not_found_in_trash'    => __( 'No Films found in Trash.', 'textdomain' ),
    'featured_image'        => _x( 'Film Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
    'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
    'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
    'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
    'archives'              => _x( 'Film archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
    'insert_into_item'      => _x( 'Insert into Film', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
    'uploaded_to_this_item' => _x( 'Uploaded to this Film', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
    'filter_items_list'     => _x( 'Filter Films list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
    'items_list_navigation' => _x( 'Films list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
    'items_list'            => _x( 'Films list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
  );
  $args = array(
    'labels'             => $labels,
    'label'              => __( 'Films', 'textdomain' ),
    'menu_icon'          => 'dashicons-editor-video',
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'film' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'taxonomies'         => array('film_category'),
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
  );
  register_post_type( 'film', $args );
}
add_action( 'init', 'wpdocs_codex_book_init' );
