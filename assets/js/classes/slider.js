import {
  tns
} from "../../../node_modules/tiny-slider/src/tiny-slider";

export default class slider {
  constructor(selector, nav) {
    this.sliderContainer = document.querySelector(selector);
    this.init(selector, nav);
  }

  init(selector, nav) {
    if (this.sliderContainer) {
      let slider = tns({
        container: selector,
        mouseDrag: true,
        nav: nav,
        loop: true,
        items: 1,
        autoplay: false,
        controls: true,
        // lazyload: true,
        responsive: {
          320: {
            items: 1
          },
          768: {
            items: 1
          },
          1366: {
            items: 1
          }
        }
      });
    }
  }
}
